<div align="center">

  # Straucat

[![pipeline status](https://gitlab.com/eldiosx/straycat/badges/main/pipeline.svg)](https://gitlab.com/eldiosx/straycat/-/commits/main)
[![Latest Release](https://gitlab.com/eldiosx/straycat/-/badges/release.svg)](https://gitlab.com/eldiosx/straycat/-/releases)

[![HEXO](https://img.shields.io/badge/Hexo-0E83CD?style=for-the-badge&logo=hexo&logoColor=white)](https://hexo.io/)
[![JS](https://img.shields.io/badge/JavaScript-F7DF1E?style=for-the-badge&logo=javascript&logoColor=black)](https://developer.mozilla.org/en-US/docs/Web/javascript)
[![DOCKER](https://img.shields.io/badge/Docker-2CA5E0?style=for-the-badge&logo=docker&logoColor=white)](https://docker.com/)

</div>

**Table of Contents**

- [GitLab page](#gitlab-group-pages)
- [Building locally](#building-locally)
- [Troubleshooting](#troubleshooting)

## GitLab page

This project's static Pages are built by [GitLab CI][ci],
following the steps defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

[Straycat Website][straycat].

Read more about [Ignisus Hosting][ignisus].

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project.
2. Use node LTS: `nvm install --lts` [Install NVM][NVM]
3. Install Hexo CLI: `npm install -g hexo-cli`
4. Install dependencies: `npm install`
5. Preview your site: `hexo server`
6. Add content
7. Generate your site (optional): `hexo generate`

Read more at Hexo's [documentation][documentation].

## Troubleshooting

1. Flatpak VsCode:

Force native terminal:

```json
"terminal.integrated.defaultProfile.linux": "bash",
"terminal.integrated.profiles.linux": {
    "bash": {
        "path": "/usr/bin/flatpak-spawn",
        "args": [
            "--env=TERM=xterm-256color",
            "--host",
            "script",
            "--quiet",
            "/dev/null"
          ]
    },
    "zsh": {
           "path": "/usr/bin/flatpak-spawn",
           "args": [
              "--env=TERM=vscode",
            "--env=SHELL=zsh",
            "--host",
            "script",
            "--quiet",
             "/dev/null"
        ]
    }
},
```

----

[ci]: https://about.gitlab.com/gitlab-ci/
[install]: https://hexo.io/docs/index.html#Installation
[documentation]: https://hexo.io/docs/
[ignisus]: https://ignisus.org/hosting
[straycat_web]: https://straycat.ignisus.org/
[NVM]: https://github.com/nvm-sh/nvm
