FROM node:latest

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY . .

RUN npx hexo generate

EXPOSE 80

CMD [ "npx", "hexo", "server", "-p", "80" ]

# docker build -t straycat .

# docker run -p 4000:80 straycat
