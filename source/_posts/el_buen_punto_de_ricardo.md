---
title: El Buen Punto de Ricardo
location: C. José Luis Luque, 3, Casco Antiguo, 41004 Sevilla
date: 2024-05-15 12:39:34
map: https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d341.28995552109245!2d-5.991164354073241!3d37.3923173428829!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd126d3553964eb9%3A0x2ef6f4120ab83902!2sEl%20Buen%20Punto%20de%20Ricardo!5e1!3m2!1sen!2ses!4v1716469111102!5m2!1sen!2ses
tags: barato
# Schedule:
monday: 06:00 PM – 06:00 AM
tuesday: 06:00 PM – 06:00 AM
wednesday: 06:00 PM – 06:00 AM
thursday: 06:00 PM – 06:00 AM
friday: 06:00 PM – 06:00 AM
saturday: 06:00 PM – 06:00 AM
sunday: 06:00 PM – 06:00 AM
---
El restaurante ideal para parar, tapear y almorzar.
Un punto de encuentro con amigos o compañeros de trabajo, disfrutando la buena comida.
