---
title: Restaurante Akira
location: C. Sta. María de Gracia, 13, Casco Antiguo, 41004 Sevilla
date: 2024-05-12 14:39:34
map: https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5460.634442620376!2d-5.999387105214358!3d37.39238386212297!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd126dd1269b602d%3A0x2846fc99710657bd!2sRestaurante%20Akira!5e1!3m2!1sen!2ses!4v1716469293915!5m2!1sen!2ses
tags: japanese
# Schedule:
monday: 13:00 PM – 23:00 PM
tuesday: 13:00 PM – 23:00 PM
wednesday: 13:00 PM – 23:00 PM
thursday: 13:00 PM – 23:00 PM
friday: 13:00 PM – 23:00 PM
saturday: 13:00 PM – 23:00 PM
sunday: 13:00 PM – 23:00 PM
---
Si eres amante de la comida japonesa auténtica, o si quieres incursionar por primera vez en las delicias de la cocina oriental.
Un punto de encuentro ideal para esa cita inolvidable.